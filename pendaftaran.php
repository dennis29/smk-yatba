
<section id="register">
			<div class="hgroup" style="width:1200">
				<h1>FORMULIR PENDAFTARAN PESERTA DIDIK BARU DAN PINDAHAN</h1>
		   </div>
		   		   <div class="row">
				<div class="signin span6" style="width:1200"> 
					<div class="form" style="width:	1200">
						<table align="center">
						<tr>
						<td width="200">Nama Sekolah</td>
						<td > : SEKOLAH MENENGAH KEJURUAN (SMK) YATBA</td>
						</tr>
						<tr>
						<td width="200">Program Studi</td>
						<td > : BISNIS MANAJEMEN</td>
						</tr>
						<tr>
						<td width="200">Program Keahlian</td>
						<td > : <b>AKUNTANSI</b></td>
						</tr>
						<tr>
						<td width="200">Alamat</td>
						<td > : Jl. Babelan Muara Bakti km.7 Kec. Babelan</td>
						</tr>
						<tr>
						<td width="200">Desa</td>
						<td > : Muara Bakti</td>
						</tr>
						<tr>
						<td width="200">Kecamatan</td>
						<td > : Babelan</td>
						</tr>
						<tr>
						<td width="200">Kabupaten</td>
						<td > : Bekasi</td>
						</tr>
						<tr >
						<td width="200">Tahun Pelajaran</td>
						<td> : 2014/2015</td>
						</tr>
						<tr >
						<td height="50"></td>
						</tr>
						</table>
						<form name="form-login" id="form-login" action="simpandatacalonsiswa.php" target="_blank">
						<table>
						<tr>
						<td width="300">A. Keterangan Peserta Didik</td>
						</tr>
						<tr>
						<td width="300">1. Nama Lengkap</td>
						<td ><input id="nama" name="nama" required="" class="input-xlarge" type="text" style="height:30px"></td>
						</tr>
						<tr>
						<td width="300">2. Tempat / Tgl Lahir (Tgl-Bln-Thn)</td>
						<td><input id="tempat" name="tempat" required="" class="input-xlarge" type="text" style="height:30px"></td>
						<td><input id="tgllahir" name="tgllahir" required="" type="text" style="height:30px"></td>
						</tr>
						<tr>
						<td width="300">3. Jenis Kelamin</td>
						<td width="300"><select name="jns_kel" required=""  >
						<option value="">== ==</option>
						<option value="laki-laki">Laki - Laki</option>
						<option value="perempuan">Perempuan</option>
						</select></td>
						</tr>
						<tr>
						<td width="300">4. Anak Ke</td>
						<td width="50"><input id="anakke" name="anakke" required="" type="text" style="height:30px"></td>
						<td> Dari </td>
						<td width="50"><input id="jmlsaudara" name="jmlsaudara" required="" type="text" style="height:30px"></td>
						<td width="50"></td>
						<td> Bersaudara </td>
						</tr>
						<tr>
						<td width="300">5. Alamat</td>
						<td><textarea id="alamat" name="alamat" required="" class="input-xlarge" /></textarea></td>
						</tr>
						<tr >
						<td height="50"></td>
						</tr>	
						</table>
						<table>
						<tr>
						<td width="300">B. Keterangan Orang Tua/Wali</td>
						</tr>
						<tr>
						<td width="300">1.Nama Ayah</td>
						<td><input id="namaayah" name="namaayah" required="" class="input-xlarge" type="text" style="height:30px"></td>
						</tr>
						<tr>
						<td width="300">2. Nama Ibu</td>
						<td><input id="namaibu" name="namaibu" required="" class="input-xlarge" type="text" style="height:30px"></td>
						</tr>
						<tr>
						<td width="300">3. Pendidikan Ayah</td>
						<td><select name="pendidikanayah" required="">
						<option value="">== ==</option>
						<option value="tidaksekolah">Tidak Sekolah</option>
						<option value="sdmi">SD/MI</option>
						<option value="smpmts">SMP/Mts</option>
						<option value="smusmkman">SMU/SMK/MAN</option>
						<option value="diploma">Diploma</option>
						<option value="s1">S1</option>
						<option value="s2">S2</option>
						<option value="s3">S3</option>
						</select></td>
						</tr>
						<tr>
						<td width="300">4. Pendidikan Ibu</td>
						<td><select name="pendidikanibu" required="">
						<option value="">== ==</option>
						<option value="tidaksekolah">Tidak Sekolah</option>
						<option value="sdmi">SD/MI</option>
						<option value="smpmts">SMP/Mts</option>
						<option value="smusmkman">SMU/SMK/MAN</option>
						<option value="diploma">Diploma</option>
						<option value="s1">S1</option>
						<option value="s2">S2</option>
						<option value="s3">S3</option>
						</select></td>
						</tr>
						<tr>
						<td width="300">5. Pekerjaan Ayah</td>
						<td><select name="pekerjaanayah" required="">
						<option value="">== ==</option>
						<option value="buruh">Buruh</option>
						<option value="karyawanswasta">Karyawan Swasta</option>
						<option value="lainlain">Lain - Lain</option>
						<option value="nelayan">Nelayan</option>
						<option value="perangkatdesa">Perangkat Desa</option>
						<option value="pns">PNS</option>
						<option value="tani">Tani</option>
						<option value="tnipolri">TNI/Polri</option>
						<option value="wiraswasta">Wiraswasta</option>
						</select></td>
						</tr>
						<tr>
						<td width="300">6. Pekerjaan Ibu</td>
						<td><select name="pekerjaanibu" required="">
						<option value="">== ==</option>
						<option value="buruh">Buruh</option>
						<option value="karyawanswasta">Karyawan Swasta</option>
						<option value="lainlain">Lain - Lain</option>
						<option value="nelayan">Nelayan</option>
						<option value="perangkatdesa">Perangkat Desa</option>
						<option value="pns">PNS</option>
						<option value="tani">Tani</option>
						<option value="tnipolri">TNI/Polri</option>
						<option value="wiraswasta">Wiraswasta</option>
						</select></td>
						</tr>
						<td width="300">7. No Telepon</td>
						<td><input id="notelp_ortu" name="notelp_ortu" required="" class="input-xlarge" type="text" style="height:30px"></td>
						</tr>
						<tr >
						<td height="50"></td>
						</tr>	
						</table>
						<table>
						<tr>
						<td width="300">C. Sekolah Asal Peserta Didik</td>
						</tr>
						<tr>
						<td width="300">1. Nama Sekolah</td>
						<td><input id="namasekolah" name="namasekolah" required="" class="input-xlarge" type="text" style="height:30px"></td>
						</tr>
						<tr>
						<td width="300">2. Kelas [Jika Pindahan]</td>
						<td><input id="kelasjikapindahan" name="kelasjikapindahan" required="" class="input-xlarge" type="text" style="height:30px"></td>
						</tr>
						<tr>
						<td width="300">3. Alamat Sekolah</td>
						<td><textarea id="alamatsekolah" name="alamatsekolah" required="" class="input-xlarge" /></textarea></td>
						</tr>
						<tr>
						<td width="300">4. No Pes UN SMP/MTs</td>
						<td><input id="no_un" name="no_un" required="" class="input-xlarge" type="text" style="height:30px"></td>
						</tr>
						<tr>
						<td width="300">5. No SKHUN & Ijazah</td>
						<td><input id="noijazah" name="noijazah" required="" class="input-xlarge" type="text" style="height:30px"></td>
						</tr>
						<td height="200"><button type="submit" class="btn btn-primary btn-large">Daftar</button></td>
						</tr>	
						</table>
							</div >
							
						</form>
					</div>
					<div id="notif-login-msg"></div>
				</div>   
		   </div>
	  </section> 