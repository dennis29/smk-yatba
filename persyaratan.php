<section id="blog">
		   <div class="hgroup">
				<h1>Persyarataan PSB</h1>
		   </div>
			<div class="row">
                    <div id="leftcol" class="span8">
                         <article class="post">
                              <div class="post_content">
								<h3><span style="color: #632423;">Persyaratan Akademik Calon Siswa SMK :&nbsp;</span></h3><p><span style="color: #e36c09;">Seleksi
 berdasarkan pada scoring (pemberian nilai) yang terdiri dari tes 
akademik berupa tes Matematika, Fisika, Komputer Dasar, Bahasa 
Indonesia, Bahasa Inggris, dan Tes Warna</span></p><h3><span style="color: #632423;">Prosedur Penerimaan Siswa Baru</span></h3><p></p><ol><li><span style="line-height: 21px; "><span style="color: #953734;">Pembelian formulir dapat dilakukan dilokasi sekolah</span></span></li><li><span style="line-height: 21px;"><span style="color: #953734;">Mengisi formulir dengan lengkap dan benar sesuai dengan petunjuk pengisian</span></span></li><li><span style="line-height: 21px;"><span style="color: #953734;">Pengembalian
 formulir yang telah diisi dengan lengkap dan benar harus dikembalikan 
ke sekolah dilengkapi dengan persyaratan administrasi</span></span></li><li><span style="line-height: 21px;"><span style="color: #953734;">Tes Masuk Calon Siswa Baru mengikuti tes masuk disekolah tempat pengembalian formulir&nbsp;</span></span></li><li><span style="line-height: 21px;"><span style="color: #953734;">Bagi siswa yang lulus tes seleksi melakukan wawancara, daftar ulang dan tes narkoba disekolah</span></span></li></ol><h3><span style="color: #17365d;">Persyaratan Administrasi</span></h3><p></p><ul><li><span style="line-height: 21px; "><span style="color: #548dd4;">2 lembar fotocopy ijazah/STTB (SMP)</span></span></li><li><span style="line-height: 21px;"><span style="color: #548dd4;">2 lembar fotocopy SKHUN<br></span></span></li><li><span style="line-height: 21px;"><span style="color: #548dd4;">2 lembar fotocopy raport dari kelas 1 s/d 3<br></span></span></li><li><span style="line-height: 21px;"><span style="color: #548dd4;">Fotocopy akta kelahiran / Surat Kenal Lahir 2 lembar</span></span></li><li><span style="line-height: 21px;"><span style="color: #548dd4;">Fotocopy kartu pelajar dan NISN (1 lembar)</span></span></li><li><span style="line-height: 21px;"><span style="color: #548dd4;">3 lembar pas photo terbaru ukuran 3x4 dan 2x3 Background merah</span></span></li><li><span style="line-height: 21px;"><span style="color: #548dd4;">Surat Keterangan dari sekolah asal</span></span></li><li><span style="line-height: 21px; "><span style="color: #548dd4;">Surat Keterangan Kelakuan Baik</span></span></li><li><span style="line-height: 21px; "><span style="color: #548dd4;">Surat Keterangan Sehat</span></span></li></ul><p><span style="color: #548dd4;"><br></span></p><p></p><p></p><p><span style="color: #548dd4;"><img src="SMK%20TELEKOMUNIKASI%20TELESANDI%20-%20Persyarataan%20PSB_files/16899900-cartoon-of-girl-student-studying--reading-book.jpg" style="width: 192px; height: 144px; "><img src="SMK%20TELEKOMUNIKASI%20TELESANDI%20-%20Persyarataan%20PSB_files/student_clipart.gif" style="width: 257.62711864406776px; height: 152px; "><br></span></p><p></p>
							  </div>
                         </article>
						 						                     </div>
               </div>
		</section>