<section id="blog">
		   <div class="hgroup">
				<h1>Visi &amp; Misi SMK Telekomunikasi Telesandi</h1>
		   </div>
			<div class="row">
                    <div id="leftcol" class="span8">
                         <article class="post">
                              <div class="post_content">
								<b>VISI:</b><br>Menjadi Sekolah Kejuruan yang unggul dalam 
pengembangan sumber daya manusia dengan kompetensi utama di bidang 
Teknologi Informasi dan Telekomunikasi agar mampu mneghasilkan lulusan 
yang berkualitas tinggi, dan siap terjun ke dunia kerja, baik yang rutin
 maupun kontekstual dengan dilandasi akhlak yang mulia dan bertanggung 
jawab kepada Tuhan Yang Maha Esa.<br><br><b>MISI:</b><br><ol><li>Menyelengggarakan pendidikan Teknologi Telekomunikasi dan Informasi</li><li>Mengembangkan suasana akademis yang mendukung kebutuhan pengguna lulusan</li><li>Pemberdayaan laboratorium bahasa dan komputer untuk menunjang siswa mempunyai skill yang memadai</li><li>Mengembangkan kurikulum sesuai kompetensi yang diharapkan oleh pengguna lulusan</li><li>Segala aktifitas dilandasi nilai-nilai agama</li></ol><br>
							  </div>
                         </article>
					</div>
               
               </div>
		</section>